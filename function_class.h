
#pragma once
#include <cstdio>
#include <iostream>
#include <string>
#include <map> 
#include <fstream>
#include <typeinfo>
#include <vector>
#include <cstdlib>
#ifdef WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif // win32
using namespace std;
void cclear();
void buf_clear();
void wait_enter();
void get_str(string *c);
class worker
{
private:
	string firstname;
	string secondname;
	string thname;
	string sex;
	int age;
	int zp;
	int hours_per_day;
public:
	worker(string fn, string sn, string tn, string s, int a, int z, int hpd);
	worker();
	void s_firstname(string fn);
	void s_secondname(string sn);
	void s_thname(string tn);
	void s_sex(string tmp);
	void s_age(int tmp);
	void s_zp(int tmp);
	void s_hours_per_day(int tmp);
	const string g_firstname();
	const string g_secondname();
	const string g_thname();
	const string g_sex();
	const int g_age();
	const int g_zp();
	const int g_hours_per_day();
	virtual void show();

};
class database
{
public:
	multimap <string, worker *> base;
	void add_worker();
	void show_all();
	void write_file();
	void read_file();
	vector <multimap <string, worker *>::iterator> *find();
	void show_finded();
	void delete_worker();
};
class administration : public worker
{
public:
	administration(string fn, string sn, string tn, string s, int a, int z, int hpd);
	administration();
	void show();
};
class low_level_worker : public worker
{
public:
	low_level_worker(string fn, string sn, string tn, string s, int a, int z, int hpd);
	low_level_worker();
	void show();
};
