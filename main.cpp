#include "function_class.h"

int main()
{
	database base1;
	int x;
	base1.read_file();
	while (1)
	{
		cout << "1. Add worker.\n" << "2. Find worker.\n" << "3. Delete worker.\n" << "4. Shows all workers.\n" << "5. Exit\n";
		cout << "Enter: ";
		buf_clear();
		cin >> x;
		if (x == 1)
		{
			cclear();
			base1.add_worker();
			cout << "Added!\n";
			wait_enter();
		}
		else if (x == 2)
		{
			cclear();
			base1.show_finded();
			wait_enter();
		}
		else if (x == 3)
		{
			cclear();
			base1.delete_worker();
			wait_enter();
		}
		else if (x == 4)
		{
			cclear();
			base1.show_all();
			cin.ignore();
			wait_enter();
		}
		else if (x == 5)
		{
			base1.write_file();
			exit(0);
		}
		x = 0;
		cclear();
	}
	return 0;
}
