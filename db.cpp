#include "function_class.h"
//�����, �� ���� ���������� ����� ������. 
//������� �������� � multimap, ������ �������� �������� ��� ��������, � ������ ����� - ��������� �� ���������� ��������
void database::add_worker()//������� ���������� �������� � ����
{
	string firstname;//����������� � ������������ ��� ������
	string secondname;
	string thname;
	string sex;
	int age;
	int zp;
	int hours_per_day;
	int pointer;
	cout << "Enter first name:";
	cin >> firstname;
	cout << "Enter second name:";
	cin >> secondname;
	cout << "Enter third name:";
	cin >> thname;
	cout << "Enter sex:";
	cin >> sex;
	cout << "Enter age:";
	cin >> age;
	while (!(age <100 && age >0))
	{
		cout << "Invalid input! Enter age: ";
		buf_clear();
		cin >> age;
	}
	cout << "Enter wage:";
	cin >> zp;
	cout << "Enter hours per day:";
	cin >> hours_per_day;
	while (!(hours_per_day > 0 && hours_per_day <10))
	{
		cout << "Invalid input! Enter hours per day: ";
		buf_clear();
		cin >> hours_per_day;
	}
	cout << "Select class (1 - Administartion or 2 - Other): ";
	cin >> pointer;
	while ((pointer != 1) && (pointer !=2))
	{
		cout << "Invalid input! Select class (1 or 2): ";
		buf_clear();
		cin >> pointer;
	}
	if (pointer == 1) 
	{ 
		administration* wk = new administration(firstname, secondname, thname, sex, age, zp, hours_per_day); 
		base.insert(std::pair<string, worker *>(firstname + " " + secondname + " " + thname, (worker *)wk));//��������� �������� � ������ (���, ��������� �� ��������)
	}
	else 
	{ 
		low_level_worker* wk = new low_level_worker(firstname, secondname, thname, sex, age, zp, hours_per_day); 
		base.insert(std::pair<string, worker *>(firstname + " " + secondname + " " + thname, (worker *)wk));
	}
}

void database::show_all()//��������� �������� �� ����� �������, ������ �� ����� ������ �� ���������, ������ �����������
{
	worker *wk;
	cout << "All workers:\n\n";
	for (auto it = base.begin(); it != base.end(); ++it)
	{
		wk = it->second;
		wk->show();
	}
	buf_clear();
}
//������ � ����. �� �� ����� �������� � ���� ��� ������ ���� (�������� ������)
//������� � ���� ���������� ����� ���� ������� � �������� ����
void database::write_file()//������ � ����. 
{
	worker *wk;
	ofstream out("base", ios::out | ios::binary);
	if (!out.is_open())
		cout << "File not open!\n";
	char a = 'A';
	char b = 'O';
	out << base.size();
	for (auto it = base.begin(); it != base.end(); ++it)
	{
		wk = it->second;
		if (typeid(*wk) == typeid(administration))
		{
			out.write((char*)(&a), sizeof (char));
			out.write((char*)(it->second), sizeof (administration));
		}
		else
		{
			out.write((char*)(&b), sizeof (char));
			out.write((char*)(it->second), sizeof (low_level_worker));
		}
	}
	out.close();
}
//��������� �� ������� ������� worker � ��� �������, � ������� ��� ���� ��������
//��������� ��������� �� ��������� ������� � multimap, ��������� ���� �� ������ ���� �����
void database::read_file()
{
	worker *wk;
	administration *ad;
	low_level_worker *low;
	char buf;
	int numb;
	ifstream in("base", ios::in | ios::binary);
	if (!in.is_open())
	{
		cout << "File not open!\n";
		return;
	}
	in >> numb;
	for (int i = 0; i < numb;i++)
	{
		in.read((char*)&buf, sizeof (char));
		if (buf == 'A')
		{
			ad = new administration();
			in.read((char*)ad, sizeof (administration));
			base.insert(std::pair<string, worker *>(ad->g_firstname() + " " + ad->g_secondname() + " " + ad->g_thname(), ad));
		}
		else
		{
			low = new low_level_worker();
			in.read((char*)low, sizeof (low_level_worker));
			base.insert(std::pair<string, worker *>(low->g_firstname() + " " + low->g_secondname() + " " + low->g_thname(), low));
		}
	}
	in.close();
}
vector <multimap <string, worker *>::iterator> * database::find()//����� (��� ������ �� �����)
{
	vector <multimap <string, worker *>::iterator> *finded = new vector <multimap <string, worker *>::iterator>;//������ ������, ��� � ��� ���������
//�� ��� ��������!
//������� ������ �� ���������� �multimap
	string tmp = {""};
	int i=0;
	buf_clear();
	cout << "Enter first or second or third name:\n";
	buf_clear();
	buf_clear();
	buf_clear();
	cin.ignore();
	get_str(&tmp);
//	cin>>tmp;
	for (auto it = base.begin(); it != base.end(); ++it)
	{
		if ((it->first).find(tmp) != string::npos)//���������� ������, ������� ���� � ������. ������������ ��� ���, ��� � �������, � ��������. 
		{                                         //Vlad  � Vladimir ���� � �� ��. �� ���, � ����!
			finded->push_back(it);
			i++;
		}
	}
	if (i == 0){ cout << "Not find!\n"; }
	else { cout << "Find " << i << " workers!\n"; }
	return finded;//���������� ������ �� ��������� ������ ���������� multimap � ������������ �������
}
void database::show_finded()//���������� ���� � ������� ���������� � ������� �� �����
{
	vector <multimap <string, worker *>::iterator> *tmp;
	tmp = find();
	worker *wk;
	administration *ad;
	low_level_worker *low;
	for (auto it = tmp->begin(); it != tmp->end(); ++it)
	{
		wk = (*it)->second;
		wk->show();
	}
	delete tmp;
}
void database::delete_worker()//�������� ��������
{
	vector <multimap <string, worker *>::iterator> *tmp;
	tmp = find();
	worker* tmp2;
	for (auto it = tmp->begin(); it != tmp->end(); ++it)
	{
		tmp2=(*it)->second;//��������
		base.erase(*it);//��������
		tmp2->show();//������� ��, ��� ����� �������
		delete tmp2;		//����� ��� ������ ������?? ��� ����� ���������� ���������, ������ ����� ������!!!
	}
	delete tmp;
}
