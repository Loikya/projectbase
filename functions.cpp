#include "function_class.h"

void cclear()
{
#ifdef WIN32
	system("cls");
#else
	system("clear");
#endif // win32
}
void buf_clear()
{
	cin.clear();
	cin.sync();
}
void wait_enter()
{
	buf_clear();
	cout << "Press enter:";
	cin.get();
}

void get_str(string *c) 
{ 
	char ch; 
	while ((ch = getchar()) != '\n') 
	{ c->push_back(ch); } 
}
